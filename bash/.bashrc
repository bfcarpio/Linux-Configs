stty -ixon

export PS1="\[$(tput bold)\]\[$(tput setaf 1)\][\[$(tput setaf 3)\]\u\[$(tput setaf 2)\]@\[$(tput setaf 4)\]\h \[$(tput setaf 5)\]\W\[$(tput setaf 1)\]]\[$(tput setaf 7)\]\\$ \[$(tput sgr0)\]"

# cat ~/.cache/wal/sequences

shopt -s autocd #Allows you to cd into directory merely by typing the directory name.


#Generic shortcuts:
alias music="ncmpcpp"
alias clock="ncmpcpp -s clock"
alias visualizer="ncmpcpp -s visualizer"
alias news="newsboat"
alias email="mutt"
alias files="ranger"
alias chat="weechat"
alias audio="ncpamixer"
alias calender="calcurse"

#Some aliases
alias setclip="xclip -selection c"
alias getclip="xclip -selection c -o"
alias mirror="sudo reflector --verbose --latest 50 --protocol https --sort rate --save /etc/pacman.d/mirrorlist"
alias cv="zathura ~/Documents/Resume/Resume.pdf"
alias v="vim"
alias ka="killall"
alias sv="sudo vim"
alias fs="ranger"
alias ls="exa"
# alias ls='ls -hN --group-directories-first'
# alias ls='ls -hN --color=auto --group-directories-first'
alias g="git"
alias gitup="git push origin master"
alias mkdir="mkdir -pv"
alias crep="grep --color=always"
alias p="sudo pacman"
alias sdn="sudo shutdown now"
alias yt="youtube-dl"
alias yta="youtube-dl -x -f bestaudio"
alias starwars="telnet towel.blinkenlights.nl"
alias nf="clear && neofetch"
alias newnet="sudo systemctl restart NetworkManager"
alias youtube="mpsyt"
alias YT="mpsyt"
alias syt="mpsyt"
alias Txa="cp ~/Documents/LaTeX/article.tex"
alias Txs="cp ~/Documents/LaTeX/beamer.tex"
alias Txh="cp ~/Documents/LaTeX/handout.tex"
alias TC='texclear'
alias mailsync="bash ~/.config/Scripts/mailsyncloop"
# alias tr="transmission-remote"
# alias debase="sudo umount /home/Shared/Videos & screen.sh l"
alias speedvid="bash ~/.config/Scripts/speedvid.sh"
# alias bw="wal -i ~/.config/wall.png"
alias slack="slack-term"

weather() { curl wttr.in/$1?m?n ;}

get_recipe()
{ 
	curl -sG "https://plainoldrecipe.com/recipe" -d "url=${1}" | pandoc -f html -t markdown
}

#Music
alias pause="mpc toggle"
alias next="mpc next"
alias prev="mpc prev"
alias trupause="mpc pause"
alias beg="mpc seek 0%"
alias lilbak="mpc seek -10"
alias lilfor="mpc seek +10"
alias bigbak="mpc seek -120"
alias bigfor="mpc seek +120"

[ -f /usr/share/fzf/key-bindings.bash ] && source /usr/share/fzf/key-bindings.bash
[ -f /usr/share/fzf/completion.bash ] && source /usr/share/fzf/completion.bash

source ~/.shortcuts
source ~/.bash-powerline.sh

# Use `fd` to search with FZF
# FAST!!
export FZF_DEFAULT_COMMAND="fd -E ~/go --follow ."
export FZF_CTRL_T_COMMAND="$FZF_DEFAULT_COMMAND"
export FZF_ALT_C_COMMAND="fd -E ~/go --follow -t d ."

export PATH=$PATH:/home/bfcarpio/go/bin

. $HOME/.asdf/asdf.sh

. $HOME/.asdf/completions/asdf.bash
