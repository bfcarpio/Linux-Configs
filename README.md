# README
Initial setup of files comes from [Larbs.xyz installer from Luke Smith](https://github.com/LukeSmithxyz/voidrice).

## Notes
+ For Brother HL-L2300D install the dedicated driver package and or `brlaser`. Make sure to add user to `sys` group
+ Ethernet: Make sure to install the `r8168` drivers just in case
+ Disabled `NetworkManager-wait-online.service` and `Docker.service`. No reason the computer can't boot without internet connection and I don't use docker constantly.
+ For "open here in terminal" in nemo: `gsettings set org.cinnamon.desktop.default-applications.terminal exec kitty`
