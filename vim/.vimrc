if &compatible
  set nocompatible
endif
filetype off
" append to runtime path
set rtp+=/usr/share/vim/vimfiles
" initialize dein, plugins are installed to this directory
call dein#begin(expand('~/.cache/dein'))
" add packages here, e.g:
call dein#add('editorconfig/editorconfig-vim')
call dein#add('sheerun/vim-polyglot')
call dein#add('tpope/vim-surround')
call dein#add('tpope/vim-repeat')
call dein#add('jiangmiao/auto-pairs')
call dein#add('tpope/vim-commentary')
call dein#add('gabrielelana/vim-markdown')
call dein#add('junegunn/vim-easy-align')
call dein#add('nelstrom/vim-visual-star-search')
call dein#add('michaeljsmith/vim-indent-object')
call dein#add('tpope/vim-eunuch')
call dein#end()
" auto-install missing packages on startup
if dein#check_install()
  call dein#install()
endif
filetype plugin indent on

let g:EditorConfig_exclude_patterns = ['fugitive://.*','scp://.*']
" disable python 2 support
let g:loaded_python_provider = 1
let g:python3_host_prog = '/usr/bin/python'

" Show numbers on the side
set number
syntax on
set autoindent
set backspace=indent,eol,start
set incsearch
set scrolloff=1
set ruler
set autoread

nnoremap <C-t> :tabnew<cr>
map <C-h> <C-w>h
let g:Ctrl_j = 'off'
map <C-j> <C-w>j
map <C-k> <C-w>k
map <C-l> <C-w>l
nmap <C-_> gcc

" for command mode
nnoremap <S-Tab> <<
" for insert mode
inoremap <S-TAB> <C-d>

" Move lines up and down with Alt
nnoremap <A-j> :m+<CR>==
nnoremap <A-k> :m-2<CR>==
inoremap <A-j> <Esc>:m+<CR>==gi
inoremap <A-k> <Esc>:m-2<CR>==gi
vnoremap <A-j> :m'>+<CR>gv=gv
vnoremap <A-k> :m-2<CR>gv=gv

" Highlight search results
set hlsearch
" Incremental search, search as you type
set incsearch 
" Ignore case when searching 
set ignorecase 
" Ignore case when searching lowercase
set smartcase
" Stop highlighting on Enter
map <CR> :noh<CR>

" Easy Align
nmap ga <Plug>(EasyAlign)

